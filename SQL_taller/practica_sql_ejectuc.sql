set @o_return = 0;

/*select from cliente*/

/*Insertar un registro*/
call bd_test.sp_cliente_TV('C1', 'Virgilio', 'Vega', '8-999-000', 'M', '1995-03-10', @o_return);
/*Consulta por id de cliente*/
call bd_test.sp_cliente_TV('R1',1, null, null, null, null, null, @o_return);
/*Consulta por cédula*/
call bd_test.sp_cliente_TV('R2',null, null, null, '8-789-132', null, null, @o_return);
/*Consulta 1000 registros*/
call bd_test.sp_cliente_TV('R3', null,null, null, null, null, null, @o_return);

/*Actualiza Registro*/
call bd_test.sp_cliente('R1', 4000, null, null, null, null, null, @o_return); /*Verifico valores anteriores*/
call bd_test.sp_cliente('U1',22, 'virgilio2', 'Vega2', '8-999-001', 'M', '1991-01-12', @o_return); /*Actualizo valores*/
call bd_test.sp_cliente('R1', 4000, null, null, null, null, null, @o_return); /*Verifico nuevos valores*/

select * from bd_test.cliente_TV

/*Agregar hobbies al cliente*/
CALL bd_test.sp_cliente_hobbie('C1',1000,3, null, @o_return);
CALL `bd_test`.`sp_consulta_cliente_hobbies`(1000);

Call bd_test.sp_cliente_TV('D1', 4, null, null, null, null,null, @o_return);