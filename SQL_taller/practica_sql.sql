USE bd_test

CREATE TABLE bd_test.cliente_vv (
    id_cliente int NOT NULL AUTO_INCREMENT,
	nombre varchar(200) NOT NULL,
	apellido varchar (64) NULL,
    cedula varchar(64) NOT NULL,
	sexo char(1) NULL,
    fecha_nacimiento datetime NOT NULL,
    PRIMARY KEY (id_cliente),
    UNIQUE INDEX idx_unique_cedula (cedula)
    )

comment 'tabla de Clientes de Virgilio V';

CREATE TABLE bd_test.cliente_hobbie_vv(
    id_cliente int NOT NULL,
    id_hobbie int NOT NULL,
    primary key (id_cliente,id_hobbie)
    )
    comment 'Tabla de hobbies de cliente';

CREATE TABLE bd_test.hobbie_vv(
    id_hobbie int NOT NULL AUTO_INCREMENT,
    descripcion varchar (64) NOT NULL,
    primary key (id_hobbie)
    )
    comment 'Tabla de hobbies virgilio v';
    
insert into bd_test.cliente_vv (nombre, apellido, cedula, fecha_nacimiento) values ("
juan", "Vargas", "8-777-444", "1999-5-23");

insert into bd_test.cliente_vv (sexo) value ('m')  where id_cliente = 1;

insert into bd_test.cliente_vv (nombre, apellido, cedula, fecha_nacimiento) values ("
Pedro", "NoVargas", "8-334-4324", "1939-7-23");

insert into bd_test.cliente_vv (nombre, apellido, cedula, fecha_nacimiento) values ("
Rodrigo", "Rodriguez", "12-231-4232", "1999-5-23");


insert into bd_test.hobbie_vv (descripcion) values ("SUPER FUTBOL");

insert into bd_test.hobbie_vv (descripcion) values ("Bailar la macarena");

insert into bd_test.hobbie_vv (descripcion) values ("Usar DP en Neutral");


insert into bd_test.cliente_hobbie_vv (id_cliente, id_hobbie) values (1,2);

insert into bd_test.cliente_hobbie_vv (id_cliente, id_hobbie) values (2,3);

insert into bd_test.cliente_hobbie_vv (id_cliente, id_hobbie) values (3,1);